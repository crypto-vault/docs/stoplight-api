![header](../assets/images/crypto-vault.png)

> **Blockchain Wallet API** (*a-ka* <code>CryptoVault</code>) provides a simple but yet powerful and secure way to interact with cryptocurrencies and assets via code (receive / send / hodl)

### Getting Started
1. [Register service](GettingStarted/RegisterService.md)
2. [Create wallet](GettingStarted/CreateWallet.md)
3. [Request balance](GettingStarted/RequestBalance.md)
4. [Create transaction](GettingStarted/CreateTransaction.md)
5. [Seal vault](GettingStarted/SealVault.md)

</br>

---

#### Disclaimer
<text>
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
</text>